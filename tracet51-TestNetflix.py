#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, netflix_get_customer_rating, netflix_get_movie_rating, netflix_predict, RMSE
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):

        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()

        netflix_eval(r, w)

        self.assertEqual(
            w.getvalue(), "10040:\n3.0687100355799997\n2.87671003558\n3.5627100355799994\n0.78\n")

    def test_eval_2(self):

        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()

        netflix_eval(r, w)

        self.assertEqual(
            w.getvalue(), "1:\n3.77971003558\n3.37971003558\n3.68671003558\n0.54\n")
            
    def test_eval_3(self):

        r = StringIO("1000:\n97460\n977808\n1010534\n")
        w = StringIO()

        netflix_eval(r, w)

        self.assertEqual(
            w.getvalue(), "1000:\n3.96671003558\n2.9677100355799997\n2.67971003558\n0.71\n")
    # ----
    # predict
    # ----
    def test_netflix_predict_floats(self):
        s = netflix_predict(3.76, 2.913)
        self.assertEqual(s, 3.0687100355799997)
    
    def test_netflix_predict_small_ints(self):
        s = netflix_predict(1, 4)
        self.assertEqual(s, 1.39571003558)

    def test_prediction_large_nums(self):
        s = netflix_predict(3420944.254, 9424954.559)
        self.assertEqual(s, 5.0)

    def test_prediction_min_score(self):
        s = netflix_predict(1.0, 1.0)
        self.assertEqual(s, 1.0)


    # ----
    # Get Movie Rating
    # ----
    def test_get_movie_rating_1(self):
        movie_id = 10040
        s = netflix_get_movie_rating(movie_id)
        self.assertEqual(s,2.913)

    def test_get_movie_rating_2(self):
        movie_id = 1
        s = netflix_get_movie_rating(movie_id)
        self.assertEqual(s,3.75)
    
    def test_get_movie_rating_3(self):
        movie_id = 2
        s = netflix_get_movie_rating(movie_id)
        self.assertEqual(s,3.559)

    
    

    # ----
    # Get Customer Rating
    # ----

    def test_get_customer_rating_1(self):
        customer_id = 2417853
        s = netflix_get_customer_rating(customer_id)
        self.assertEqual(s, 3.76)

    def test_get_customer_rating_2(self):
        customer_id = 1207062
        s = netflix_get_customer_rating(customer_id)
        self.assertEqual(s, 3.568)

    def test_get_customer_rating_3(self):
        customer_id = 2487973
        s = netflix_get_customer_rating(customer_id)
        self.assertEqual(s, 4.254)

    # ----
    # RMSE
    # ----

    def test_rmse_basic(self):
        predictions = [1,2,3]
        actual = [3,2,1]
        s = RMSE(predictions, actual)
        self.assertEqual(s, (8/3)**(1/2))

    def test_rmse_complex(self):
        predictions = [1.122, 2.345, 3.413]
        actual = [4.256, 3.653, 1.087]
        s = RMSE(predictions, actual)
        self.assertEqual(s, 2.3764887263916625)

    def test_rmse_large(self):
        predictions = [1928, 3483, 2484]
        actual = [9485, 7473, 5348]
        s = RMSE(actual, predictions)
        self.assertEqual(s, 5203.551511547987)

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
